﻿using EatAndFeelGoodMVC.Models;
using EatAndFeelGoodMVC.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EatAndFeelGoodMVC.Controllers
{
    public class ReviewController : Controller
    {
        private readonly ReviewServices _reviewService;

        public ReviewController(ReviewServices reviewService)
        {
            _reviewService = reviewService;
        }
        public ActionResult Review()
        {
            var rev = _reviewService.GetReview();
            var ord = _reviewService.SortReview(rev);
            return View(ord);
        }
        // GET: Homework
        public ActionResult AddReview()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddReview(Review review)
        {
            try
            {
                _reviewService.AddReview(review);
                _reviewService.Save();
                ModelState.Clear();
                ViewData["Message"] = "Success";
            }
            catch
            {
                ViewData["Message"] = "Error";
            }
            return View();
        }
    }
}
