﻿using EatAndFeelGoodMVC.Models;
using EatAndFeelGoodMVC.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EatAndFeelGoodMVC.Controllers
{
    public class ReservationController : Controller
    {
        private readonly ReservationServices _reservationService;

        public ReservationController(ReservationServices reservationService)
        {
            _reservationService = reservationService;
        }

        // GET: Homework
        public ActionResult AddReservation()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddReservation(Reservation reservation)
        {
            try
            {
                _reservationService.AddReservation(reservation);
                _reservationService.Save();
                ModelState.Clear();
                ViewData["Message"] = "Success";
            }
            catch
            {
                ViewData["Message"] = "Error";
            }
            return View();
        }
    }

}
