﻿using EatAndFeelGoodMVC.Models;
using EatAndFeelGoodMVC.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EatAndFeelGoodMVC.Controllers
{
    public class ClientController : Controller
    {
            private readonly ClientServices _clientService;

        public ClientController(ClientServices clientService)
        {
            _clientService = clientService;
        }

        // GET: Homework
        public ActionResult AddClient()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddClient(Client client)
        {
            try
            {
                _clientService.AddClient(client);
                _clientService.Save();
                ModelState.Clear();
                ViewData["Message"] = "Success";
            }
            catch
            {
                ViewData["Message"] = "Error";
            }
            return View();
        }

    }
}

