﻿using EatAndFeelGoodMVC.Models;
using EatAndFeelGoodMVC.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EatAndFeelGoodMVC.Controllers
{
    public class ContactController : Controller
    {
        private readonly ContactServices _contactService;

        public ContactController(ContactServices contactService)
        {
            _contactService = contactService;
        }

        // GET: Homework
        public ActionResult AddContact()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddContact(Contact contact)
        {
            try
            {
                _contactService.AddContact(contact);
                _contactService.Save();
                ModelState.Clear();
                ViewData["Message"] = "Success";
            }
            catch
            {
                ViewData["Message"] = "Error";
            }
            return View();
        }

    }
}
