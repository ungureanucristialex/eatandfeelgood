﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace EatAndFeelGoodMVC.Models
{
    public class Contact
    {
        public Guid ContactId { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }

        public int ClientId { get; set; }
        public Client Client { get; set; }

    }
}
