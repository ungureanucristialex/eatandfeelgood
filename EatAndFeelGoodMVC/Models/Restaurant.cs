﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Http;

namespace EatAndFeelGoodMVC.Models
{
    public class Restaurant
    {
        public Guid RestaurantId { get; set; }
        public string Name { get; set; }
        public string CityName { get; set; }
        public string ImageName { get; set; }

        [NotMapped]
        public IFormFile Image { get; set; }

        public int AdminId { get; set; }
        public Admin Admin { get; set; }
    }
}
