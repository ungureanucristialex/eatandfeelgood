﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace EatAndFeelGoodMVC.Models
{
    public class Review
    {
        public Guid ReviewId { get; set; }
        public string Description { get; set; }
        public int Grade { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Date")]
        public DateTime Date { get; set; }

        public int ClientId { get; set; }
        public Client Client { get; set; }
    }
}
