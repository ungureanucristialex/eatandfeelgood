﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace EatAndFeelGoodMVC.Models
{
    public class Admin:User
    {
        [StringLength(50)]
        [Display(Name = "Office Location")]
        public string Location { get; set; }
        public ICollection<Restaurant> Restaurants { get; set; }


    }
}
