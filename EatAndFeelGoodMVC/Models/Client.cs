﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace EatAndFeelGoodMVC.Models
{
    public class Client: User
    {
        public string Status { get; set; }

        public ICollection<Reservation> Reservations { get; set; }
        public ICollection<Contact> Contacts { get; set; }
        public ICollection<Review> Reviews { get; set; }
    }
}
