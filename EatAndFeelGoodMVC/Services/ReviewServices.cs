﻿using EatAndFeelGoodMVC.InterfaceRepository;
using EatAndFeelGoodMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace EatAndFeelGoodMVC.Services
{
    public class ReviewServices : BaseServices
    {
        public ReviewServices(IRepositoryWrapper repositoryWrapper)
            : base(repositoryWrapper)
        {
        }

        public List<Review> GetReview()
        {
            return repositoryWrapper.ReviewRepository.FindAll().ToList();
        }

        public List<Review> GetReviewsByCondition(Expression<Func<Review, bool>> expression)
        {
            return repositoryWrapper.ReviewRepository.FindByCondition(expression).ToList();
        }

        public IEnumerable<Review> SortReview(IEnumerable<Review> rev)
        {
            return rev.OrderBy(x => x.Grade);
        }
        public void AddReview(Review review)
        {
            Review new_review = new Review();
            new_review.Description = review.Description;
            new_review.Date = review.Date;
            new_review.Grade = review.Grade;
            repositoryWrapper.ReviewRepository.Create(new_review);
            repositoryWrapper.Save();
        }

        public void UpdateReview(Review review)
        {
            repositoryWrapper.ReviewRepository.Update(review);
        }

        public void DeleteReview(Review review)
        {
            repositoryWrapper.ReviewRepository.Delete(review);
        }

    }
}
