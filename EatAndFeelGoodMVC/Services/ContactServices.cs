﻿using EatAndFeelGoodMVC.InterfaceRepository;
using EatAndFeelGoodMVC.Models;
using EatAndFeelGoodMVC.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace EatAndFeelGoodMVC.Services
{
    public class ContactServices : BaseServices
    {
        public ContactServices(IRepositoryWrapper repositoryWrapper)
            : base(repositoryWrapper)
        {

        }

        public List<Contact> GetContact()
        {
            return repositoryWrapper.ContactRepository.FindAll().ToList();
        }

        public List<Contact> GetContactsByCondition(Expression<Func<Contact, bool>> expression)
        {
            return repositoryWrapper.ContactRepository.FindByCondition(expression).ToList();
        }

        public void AddContact(Contact contact)
        {
            Contact new_contact = new Contact();
            new_contact.Subject = contact.Subject;
            new_contact.Message = contact.Message;
            repositoryWrapper.ContactRepository.Create(new_contact);
            repositoryWrapper.Save();
        }
        public void UpdateContact(Contact contact)
        {
            throw new NotImplementedException();
        }
        public void DeleteContact(Contact contact)
        {
            throw new NotImplementedException();
        }


    }
}
