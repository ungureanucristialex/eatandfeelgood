﻿using EatAndFeelGoodMVC.InterfaceRepository;
using EatAndFeelGoodMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace EatAndFeelGoodMVC.Services
{
    public class RestaurantServices : BaseServices
    {
        public RestaurantServices(IRepositoryWrapper repositoryWrapper)
            : base(repositoryWrapper)
        {
        }

        public List<Restaurant> GetRestaurant()
        {
            return repositoryWrapper.RestaurantRepository.FindAll().ToList();
        }

        public List<Restaurant> GetRestaurantsByCondition(Expression<Func<Restaurant, bool>> expression)
        {
            return repositoryWrapper.RestaurantRepository.FindByCondition(expression).ToList();
        }

        public void AddRestaurant(Restaurant restaurant)
        {
            repositoryWrapper.RestaurantRepository.Create(restaurant);
        }

        public void UpdateRestaurant(Restaurant restaurant)
        {
            repositoryWrapper.RestaurantRepository.Update(restaurant);
        }

        public void DeleteRestaurant(Restaurant restaurant)
        {
            repositoryWrapper.RestaurantRepository.Delete(restaurant);
        }
    }
}
