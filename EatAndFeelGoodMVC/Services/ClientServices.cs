﻿using EatAndFeelGoodMVC.InterfaceRepository;
using EatAndFeelGoodMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace EatAndFeelGoodMVC.Services
{
    public class ClientServices : BaseServices
    {
        public ClientServices(IRepositoryWrapper repositoryWrapper)
            : base(repositoryWrapper)
        {
        }
        public List<Client> GetClient()
        {
            return repositoryWrapper.ClientRepository.FindAll().ToList();
        }

        public List<Client> GetClientByCondition(Expression<Func<Client, bool>> expression)
        {
            return repositoryWrapper.ClientRepository.FindByCondition(expression).ToList();
        }

        public void AddClient(Client client)
        {
            Client new_client = new Client();
            new_client.FirstName = client.FirstName;
            new_client.LastName = client.LastName;
            new_client.Email = client.Email;
            new_client.Password = client.Password;
            repositoryWrapper.ClientRepository.Create(new_client);
        }

        public void UpdateClient(Client client)
        {
            repositoryWrapper.ClientRepository.Update(client);
        }

        public void DeleteClient(Client client)
        {
            repositoryWrapper.ClientRepository.Delete(client);
        }
    }
}
