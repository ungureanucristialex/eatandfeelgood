﻿using EatAndFeelGoodMVC.InterfaceRepository;
using EatAndFeelGoodMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace EatAndFeelGoodMVC.Services
{
    public class ReservationServices : BaseServices
    {
        public ReservationServices(IRepositoryWrapper repositoryWrapper)
            : base(repositoryWrapper)
        {
        }

        public List<Reservation> GetReservation()
        {
            return repositoryWrapper.ReservationRepository.FindAll().ToList();
        }

        public List<Reservation> GetReservationsByCondition(Expression<Func<Reservation, bool>> expression)
        {
            return repositoryWrapper.ReservationRepository.FindByCondition(expression).ToList();
        }

        public void AddReservation(Reservation reservation)
        {
            Reservation new_reservation = new Reservation();
            new_reservation.Date = reservation.Date;
            new_reservation.Time = reservation.Time;
            repositoryWrapper.ReservationRepository.Create(new_reservation);
            repositoryWrapper.Save();
        }

        public void UpdateReservation(Reservation reservation)
        {
            repositoryWrapper.ReservationRepository.Update(reservation);
        }

        public void DeleteReservation(Reservation reservation)
        {
            repositoryWrapper.ReservationRepository.Delete(reservation);
        }
    }
}
