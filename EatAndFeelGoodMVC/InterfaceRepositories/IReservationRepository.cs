﻿using EatAndFeelGoodMVC.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace EatAndFeelGoodMVC.InterfaceRepository
{
    public interface IReservationRepository : IRepositoryBase<Reservation>
    {

    }
}
