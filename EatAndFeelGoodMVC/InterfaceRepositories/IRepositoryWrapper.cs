﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EatAndFeelGoodMVC.InterfaceRepository
{
    public interface IRepositoryWrapper
    {
        IUserRepository UserRepository { get; }
        IAdminRepository AdminRepository { get; }
        IClientRepository ClientRepository { get; }
        IContactRepository ContactRepository { get; }
        IRestaurantRepository RestaurantRepository { get; }
        IReservationRepository ReservationRepository { get; }
        IReviewRepository ReviewRepository { get; }

        void Save();
    }
}
