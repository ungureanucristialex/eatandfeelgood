﻿using EatAndFeelGoodMVC.InterfaceRepository;
using EatAndFeelGoodMVC.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace EatAndFeelGoodMVC.Repositories
{
    public class RepositoryContext : IdentityDbContext<IdentityUser>
    {
        public RepositoryContext(DbContextOptions options)
            : base(options)
        {
        }
            public DbSet<User> Users { get; set; }
            public DbSet<Admin> Admins { get; set; }
            public DbSet<Client> Clients { get; set; }
            public DbSet<Contact> Contacts { get; set; }
            public DbSet<Reservation> Reservations { get; set; }
            public DbSet<Review> Reviews { get; set; }
            public DbSet<Restaurant> Restaurants { get; set; }


    }
}
