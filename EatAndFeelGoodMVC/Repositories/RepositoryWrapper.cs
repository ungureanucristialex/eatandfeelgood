﻿using EatAndFeelGoodMVC.InterfaceRepository;
using EatAndFeelGoodMVC.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace EatAndFeelGoodMVC.Repositories
{
    public class RepositoryWrapper : IRepositoryWrapper
    {
        private RepositoryContext _repoContext;
        private IUserRepository _user;
        private IAdminRepository _admin;
        private IContactRepository _contact;
        private IClientRepository _client;
        private IReviewRepository _review;
        private IReservationRepository _reservation;
        private IRestaurantRepository _restaurant;

        public IUserRepository UserRepository
        {
            get
            {
                if (_user == null)
                {
                    _user = new UserRepository(_repoContext);
                }
                return _user;
            }
        }
        public IAdminRepository AdminRepository
        {
            get
            {
                if (_admin == null)
                {
                    _admin = new AdminRepository(_repoContext);
                }
                return _admin;
            }
        }
        public IClientRepository ClientRepository
        {
            get
            {
                if (_client == null)
                {
                    _client = new ClientRepository(_repoContext);
                }
                return _client;
            }
        }
        public IReviewRepository ReviewRepository
        {
            get
            {
                if (_review == null)
                {
                    _review = new ReviewRepository(_repoContext);
                }
                return _review;
            }
        }
        public IReservationRepository ReservationRepository
        {
            get
            {
                if (_reservation == null)
                {
                    _reservation = new ReservationRepository(_repoContext);
                }
                return _reservation;
            }
        }
        public IRestaurantRepository RestaurantRepository
        {
            get
            {
                if (_restaurant == null)
                {
                    _restaurant = new RestaurantRepository(_repoContext);
                }
                return _restaurant;
            }
        }
        public IContactRepository ContactRepository
        {
            get
            {
                if (_contact == null)
                {
                    _contact = new ContactRepository(_repoContext);
                }
                return _contact;
            }
        }
        public RepositoryWrapper(RepositoryContext repositoryContext)
        {
            _repoContext = repositoryContext;
        }
        public void Save()
        {
            _repoContext.SaveChanges();
        }

    }
}
