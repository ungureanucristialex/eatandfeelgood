﻿using EatAndFeelGoodMVC.InterfaceRepository;
using EatAndFeelGoodMVC.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace EatAndFeelGoodMVC.Repositories
{
    public class AdminRepository : RepositoryBase<Admin>, IAdminRepository
    {
        public AdminRepository(RepositoryContext repositoryContext)
            : base(repositoryContext)
        {


        }
    }
}
