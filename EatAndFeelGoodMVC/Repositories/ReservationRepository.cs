﻿using EatAndFeelGoodMVC.InterfaceRepository;
using EatAndFeelGoodMVC.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace EatAndFeelGoodMVC.Repositories
{
    public class ReservationRepository : RepositoryBase<Reservation>, IReservationRepository
    {
        public ReservationRepository(RepositoryContext repositoryContext)
            : base(repositoryContext)
        {


        }
    }
}
