﻿using EatAndFeelGoodMVC.InterfaceRepository;
using EatAndFeelGoodMVC.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace EatAndFeelGoodMVC.Repositories
{
    public abstract class RepositoryBase<T> : IRepositoryBase<T> where T : class
    {
        public RepositoryContext _repositoryContext { get; set; }
        public RepositoryBase(RepositoryContext repositoryContext)
        {
            _repositoryContext = repositoryContext;
        }
        public IQueryable<T> FindAll()
        {
            return this._repositoryContext.Set<T>();
        }
        public IQueryable<T> FindByCondition(Expression<Func<T,bool>>expression)
        {
            return this._repositoryContext.Set<T>().Where(expression);
        }
        public void Create(T entity)
        {
            _repositoryContext.Set<T>().Add(entity);
        }
        public void Update(T entity)
        {
            _repositoryContext.Set<T>().Update(entity);
        }
        public void Delete(T entity)
        {
            _repositoryContext.Set<T>().Remove(entity);
        }
    }

}
